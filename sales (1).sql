-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 21, 2019 at 02:41 PM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 5.6.40-6+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sales`
--

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `item_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(115) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `merk_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `kategori_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`item_id`, `nama`, `price`, `stock`, `discount`, `merk_id`, `supplier_id`, `keterangan`, `created_at`, `updated_at`, `kategori_id`, `buy`) VALUES
('2822f8c2-b48b-4327-bd96-17cee24cbc8d', 'Sirwal Jogger Abu Tua L', 65000, 8, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-16 10:50:33', '2019-04-21 06:36:01', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('2b49eea7-bb53-41fe-b0bc-3ea4e7ca3495', 'Sirwal Jogger Navy XL', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-07 14:59:07', '2019-04-07 14:59:07', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('7b6704e8-4039-4d7f-8690-5c63d9525beb', 'Sirwal Jogger Hijau Tua XL', 65000, 9, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-07 14:44:34', '2019-04-20 12:09:30', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('7f0dbf42-589e-43ae-b415-9ee1698d1ca4', 'Sirwal Jogger Navy L', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-08 01:16:04', '2019-04-08 01:16:04', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('95b660e8-1325-4fdc-ba23-ba93a771136f', 'Sirwal Jogger Hitam M', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-03-12 01:37:50', '2019-04-06 16:40:58', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('95defbb8-f7af-4ad0-b7e1-8a0d46ff741f', 'Sirwal Jogger Putih Susu XL', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-16 10:42:36', '2019-04-16 10:42:36', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('9e328368-6d72-4dcd-b929-be92177faa40', 'Sirwal Jogger Hitam XL', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-07 12:13:41', '2019-04-07 12:13:41', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('ae293320-28b5-4085-90f2-9042e5821f54', 'Sirwal Jogger Coklat Pramuka XL', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-08 01:18:42', '2019-04-08 01:18:42', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('b10c7ac5-c518-4e76-9e2d-03fc471fd995', 'Sirwal Jogger Hitam L', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-07 12:13:20', '2019-04-07 12:13:47', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('b2c8ff7b-3bff-4be1-8548-6179282c03f9', 'Sirwal Jogger Putih Susu L', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-16 10:45:46', '2019-04-16 10:45:46', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('b834f1b4-1b5b-4a79-ade2-ae2e81871748', 'Sirwal Jogger Putih Kebiruan M', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-08 01:13:51', '2019-04-16 10:27:15', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('c2bbfd8d-4ea8-4c28-8662-d13771e20ee7', 'Sirwal Jogger Abu Tua XL', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-16 10:49:12', '2019-04-16 10:49:12', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('c5ba56fc-0b74-4d3b-92cd-0191fff558fb', 'Sirwal Jogger Coklat Pramuka L', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-16 10:43:47', '2019-04-16 10:43:47', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('d885ae4f-b59c-4cb5-a4f4-e85e648afda8', 'Sirwal Jogger Putih Kebiruan XL', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-07 14:58:21', '2019-04-16 10:27:41', '2a847de1-37f9-457a-9167-13585674cc7f', 55000),
('e4c87ec2-c2ac-4c3b-be18-bf50f1e9ebed', 'Sirwal Jogger Putih Kebiruan L', 65000, 10, 0, '2d0f1397-0fc1-42b2-adfa-2d84f97980a6', '6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Sirwal Jogger', '2019-04-08 01:12:25', '2019-04-16 10:27:52', '2a847de1-37f9-457a-9167-13585674cc7f', 55000);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `kategori_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(115) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kategori_id`, `nama`, `created_at`, `updated_at`) VALUES
('2a847de1-37f9-457a-9167-13585674cc7f', 'Sirwal Jogger', '2019-03-13 06:19:59', '2019-04-20 20:45:21');

-- --------------------------------------------------------

--
-- Table structure for table `merk`
--

CREATE TABLE `merk` (
  `merk_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(115) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merk`
--

INSERT INTO `merk` (`merk_id`, `nama`, `created_at`, `updated_at`) VALUES
('2d0f1397-0fc1-42b2-adfa-2d84f97980a6', 'Al-Hanif', '2019-01-26 23:54:37', '2019-01-27 00:18:31'),
('ff2e35bb-80bc-480c-b97d-9a2077aefc6a', 'AAA', '2019-03-13 06:19:53', '2019-03-13 06:19:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_26_110432_create_table_item', 1),
(4, '2019_01_26_111133_alter_table_item', 2),
(5, '2019_01_26_111339_create_table_merk', 3),
(6, '2019_01_27_042850_create_table_supplier', 3),
(7, '2019_01_27_043640_create_table_kategori', 3),
(8, '2019_01_27_044657_alter_table_item2', 3),
(9, '2019_02_13_094200_alter_table_item3', 4),
(10, '2019_04_06_024701_create_table_penjualan', 5),
(11, '2019_04_06_030604_alter_table_penjualan', 6),
(12, '2019_04_06_081924_alter_penjualan', 7),
(13, '2019_04_19_133719_create_table_pengeluaran', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran`
--

CREATE TABLE `pengeluaran` (
  `pengeluaran_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengeluaran`
--

INSERT INTO `pengeluaran` (`pengeluaran_id`, `nama`, `harga`, `created_at`, `updated_at`) VALUES
('5ceec05e-46fe-4421-a566-dea409cc6c37', 'Test pengeluaran', 25000, '2019-04-18 17:00:00', '2019-04-18 17:00:00'),
('c82b3bce-976f-4d02-9ea1-19349b4df6d1', 'blabla', 230008, '2019-04-18 17:00:00', '2019-04-18 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `penjualan`
--

CREATE TABLE `penjualan` (
  `penjualan_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `struk` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_id` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `buy` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `laba` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `penjualan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `penjualan`
--

INSERT INTO `penjualan` (`penjualan_id`, `struk`, `item_id`, `qty`, `buy`, `price`, `laba`, `created_at`, `updated_at`, `penjualan`) VALUES
('02c4bc2c-40fe-4df8-9caf-567e50960aab', '76cbbc28-39ec-4689-a19a-e9eb9f745695', '9e328368-6d72-4dcd-b929-be92177faa40', 1, 55000, 65000, 10000, '2019-04-16 03:46:12', '2019-04-16 03:46:14', 1),
('0335029a-94fe-41dd-bf05-091d097e8912', 'ac4301a2-16fb-4bc3-b2c1-f81a71b294bc', '2b49eea7-bb53-41fe-b0bc-3ea4e7ca3495', 1, 55000, 65000, 10000, '2019-04-16 03:48:40', '2019-04-16 03:48:41', 1),
('0670a507-f603-4215-a1a5-cb0a769caa88', '52f2cb4e-f3ea-4323-84c0-0d365675c4ad', 'b2c8ff7b-3bff-4be1-8548-6179282c03f9', 2, 55000, 65000, 20000, '2019-04-16 03:46:24', '2019-04-16 03:46:28', 1),
('08e90489-a269-4760-8849-ec10a51eb40a', '2512b57d-575f-4e41-83ca-e23fff818900', '7f0dbf42-589e-43ae-b415-9ee1698d1ca4', 3, 55000, 65000, 30000, '2019-04-16 03:31:41', '2019-04-16 03:31:47', 1),
('15c41cfc-2efd-4b8f-b45f-e2498274e0b1', 'fb14d121-84ad-4bfa-a222-30828db46a7b', '2822f8c2-b48b-4327-bd96-17cee24cbc8d', 2, 55000, 65000, 20000, '2019-04-19 07:25:44', '2019-04-19 07:25:47', 1),
('167fc85f-a86e-4856-9160-06842e03375f', '5b9971c1-19fe-48f3-bec9-0fcff75c7132', '9e328368-6d72-4dcd-b929-be92177faa40', 1, 55000, 65000, 10000, '2019-04-16 03:42:57', '2019-04-16 03:42:59', 1),
('1e1a1c95-4c80-4cd0-9e97-d4d5ca5e40f5', '3f0b58a2-bdaf-4493-9918-d516b4a6daa2', '7f0dbf42-589e-43ae-b415-9ee1698d1ca4', 1, 55000, 65000, 10000, '2019-04-16 03:48:02', '2019-04-16 03:48:03', 1),
('25dbf9d3-fe7e-4c2f-8b28-8ea4ac433a1b', '5e8e17cc-85f1-4660-b379-00617740a161', 'b10c7ac5-c518-4e76-9e2d-03fc471fd995', 1, 55000, 65000, 10000, '2019-04-16 03:48:15', '2019-04-16 03:48:17', 1),
('28a336d8-3ce7-4ed0-ae80-137721c11aec', 'a0b10777-e852-4675-be63-d3465ebc56a8', '7b6704e8-4039-4d7f-8690-5c63d9525beb', 1, 55000, 65000, 10000, '2019-04-19 07:25:52', '2019-04-19 07:25:55', 1),
('32d9d152-d4ee-49cc-b510-1eb261aead40', '7ff07b43-b67e-460e-98a5-7d19a07f9163', 'ae293320-28b5-4085-90f2-9042e5821f54', 1, 55000, 65000, 10000, '2019-04-16 03:34:03', '2019-04-16 03:42:08', 1),
('3e29619c-bd7d-42b4-a359-d1c93ea5453d', 'f0ca569f-87f3-4f8e-9715-a36cd94cff45', 'c2bbfd8d-4ea8-4c28-8662-d13771e20ee7', 1, 55000, 65000, 10000, '2019-04-16 03:49:25', '2019-04-16 03:49:27', 1),
('40f7a397-2c3a-416a-842c-7dfc1ecab1e7', '046da6b3-0601-4bae-91a4-fa507c49d7ba', 'ae293320-28b5-4085-90f2-9042e5821f54', 1, 55000, 65000, 10000, '2019-04-16 03:50:08', '2019-04-16 03:50:09', 1),
('50a91abb-ae67-43c6-b0de-182c4b823ead', '54aeae17-d202-481d-838a-cf712ba38215', 'e4c87ec2-c2ac-4c3b-be18-bf50f1e9ebed', 2, 55000, 65000, 20000, '2019-04-16 03:32:03', '2019-04-16 03:32:09', 1),
('52404b2f-ac50-4836-9425-c0341e8542ca', '0ab5e5dd-13f7-49a9-9e73-05e3c418e696', '7f0dbf42-589e-43ae-b415-9ee1698d1ca4', 2, 55000, 65000, 20000, '2019-04-16 03:46:40', '2019-04-16 03:46:43', 1),
('60b4421c-bfa3-4441-8318-f9fe91006158', '046da6b3-0601-4bae-91a4-fa507c49d7ba', 'c2bbfd8d-4ea8-4c28-8662-d13771e20ee7', 1, 55000, 65000, 10000, '2019-04-16 03:50:02', '2019-04-16 03:50:09', 1),
('611d5dcb-0471-4fbc-a6b7-dbceaad8eec3', 'f0ca569f-87f3-4f8e-9715-a36cd94cff45', '9e328368-6d72-4dcd-b929-be92177faa40', 1, 55000, 65000, 10000, '2019-04-16 03:49:22', '2019-04-16 03:49:27', 1),
('61445dfb-87f7-4480-b748-6d4b5bb4af70', '046da6b3-0601-4bae-91a4-fa507c49d7ba', 'b10c7ac5-c518-4e76-9e2d-03fc471fd995', 1, 55000, 65000, 10000, '2019-04-16 03:49:53', '2019-04-16 03:50:09', 1),
('70ff0948-53d5-4085-9a35-3dd5b46d1e10', '46256e04-cb02-4baf-864e-f1a667d51641', 'b10c7ac5-c518-4e76-9e2d-03fc471fd995', 2, 55000, 65000, 20000, '2019-04-16 03:45:57', '2019-04-16 03:46:01', 1),
('7140c3a9-9173-4bb7-8717-5dd056bce2ed', '238cf69d-159d-46a9-9b29-8eba310a3882', 'b10c7ac5-c518-4e76-9e2d-03fc471fd995', 1, 55000, 65000, 10000, '2019-04-16 03:49:39', '2019-04-16 03:49:40', 1),
('7a1029f6-af85-49e5-995e-7d091e8fdc8f', '46256e04-cb02-4baf-864e-f1a667d51641', 'b2c8ff7b-3bff-4be1-8548-6179282c03f9', 1, 55000, 65000, 10000, '2019-04-16 03:45:51', '2019-04-16 03:46:01', 1),
('7d8b4e07-d7c7-483a-bac9-3ff310b741aa', '1ecb4002-dbdf-4a87-99f2-86aff961db07', '9e328368-6d72-4dcd-b929-be92177faa40', 3, 55000, 65000, 30000, '2019-04-16 03:43:09', '2019-04-16 03:43:14', 1),
('8223810b-331a-46d3-8e9c-87d78fb91cbc', '44a63bf1-5644-4fc8-940c-cbcba29575b7', 'c5ba56fc-0b74-4d3b-92cd-0191fff558fb', 1, 55000, 65000, 10000, '2019-04-16 03:45:12', '2019-04-16 03:45:16', 1),
('873e2b70-25fc-44e1-ad2f-5fa05dc8a0d0', 'ac4301a2-16fb-4bc3-b2c1-f81a71b294bc', '9e328368-6d72-4dcd-b929-be92177faa40', 1, 55000, 65000, 10000, '2019-04-16 03:48:35', '2019-04-16 03:48:41', 1),
('9ca4dce7-e6f6-4a4c-af87-8372decb5040', 'f250684a-8fd9-45bc-928d-a2c076083068', 'ae293320-28b5-4085-90f2-9042e5821f54', 1, 55000, 65000, 10000, '2019-04-16 03:32:44', '2019-04-16 03:32:46', 1),
('aaca8901-522f-4ce4-817c-788b29bd6b9b', '7ff07b43-b67e-460e-98a5-7d19a07f9163', 'b10c7ac5-c518-4e76-9e2d-03fc471fd995', 1, 55000, 65000, 10000, '2019-04-16 03:34:01', '2019-04-16 03:42:08', 1),
('b0f6d9c2-d96a-4cc7-ae34-8808ed39365a', 'a0b10777-e852-4675-be63-d3465ebc56a8', '9e328368-6d72-4dcd-b929-be92177faa40', 1, 55000, 65000, 10000, '2019-04-19 07:25:53', '2019-04-19 07:25:55', 1),
('b5d4454b-6a44-4e9d-93d1-009ae30c64c9', '04d59e35-958e-4a3f-b935-14737483364e', '2822f8c2-b48b-4327-bd96-17cee24cbc8d', 1, 55000, 65000, 10000, '2019-04-16 03:50:53', '2019-04-16 03:51:02', 1),
('bbaf37a2-77d9-4b0b-b283-85128ed4ef85', '54aeae17-d202-481d-838a-cf712ba38215', 'd885ae4f-b59c-4cb5-a4f4-e85e648afda8', 1, 55000, 65000, 10000, '2019-04-16 03:32:07', '2019-04-16 03:32:09', 1),
('dbdbceaa-adca-4477-8649-58fec7881487', 'baf63d82-932d-401b-8f01-bf3b2caa233c', 'b10c7ac5-c518-4e76-9e2d-03fc471fd995', 1, 55000, 65000, 10000, '2019-04-16 03:47:54', '2019-04-16 03:47:56', 1),
('e458bc69-0a6f-4619-8073-281f0aa8a4e2', '44a63bf1-5644-4fc8-940c-cbcba29575b7', '7f0dbf42-589e-43ae-b415-9ee1698d1ca4', 1, 55000, 65000, 10000, '2019-04-16 03:45:14', '2019-04-16 03:45:16', 1),
('e9842f8e-d3b5-4f50-ace5-84ebd7e6c487', '04d59e35-958e-4a3f-b935-14737483364e', 'c5ba56fc-0b74-4d3b-92cd-0191fff558fb', 1, 55000, 65000, 10000, '2019-04-16 03:51:00', '2019-04-16 03:51:02', 1),
('efd283a6-40ae-48fe-a9f1-89d83f6fb197', '52b4da97-d16e-4ef2-981c-2f644b1bba52', '95defbb8-f7af-4ad0-b7e1-8a0d46ff741f', 1, 55000, 65000, 10000, '2019-04-16 03:42:46', '2019-04-16 03:42:47', 1),
('fb715dcb-250c-457e-927d-61070b7fdb1b', '4610827b-7949-4a39-b452-040d280be78b', '2822f8c2-b48b-4327-bd96-17cee24cbc8d', 1, 55000, 65000, 10000, '2019-04-20 05:23:54', '2019-04-20 05:24:12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplier_id` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(115) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `nama`, `alamat`, `created_at`, `updated_at`) VALUES
('6cba6842-800c-4b9c-9168-c371fd2c75cb', 'Jakaria Kranggan', 'Kranggan', '2019-01-27 06:45:45', '2019-04-20 20:42:56');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sangcahaya.com', 'fadly@sangcahaya.com', '$2y$10$ZvEPb7hExurrniWJ/ToaVuwDFRkwwE/NbBCrzkK.Be5c0MRph7.b.', NULL, '2019-04-20 17:00:00', '2019-04-20 17:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`item_id`),
  ADD KEY `kategori_id` (`kategori_id`),
  ADD KEY `merk_id` (`merk_id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `merk`
--
ALTER TABLE `merk`
  ADD PRIMARY KEY (`merk_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD PRIMARY KEY (`penjualan_id`),
  ADD KEY `penjualan_item_id_foreign` (`item_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_ibfk_1` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`kategori_id`),
  ADD CONSTRAINT `item_ibfk_2` FOREIGN KEY (`merk_id`) REFERENCES `merk` (`merk_id`),
  ADD CONSTRAINT `item_ibfk_3` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`supplier_id`);

--
-- Constraints for table `penjualan`
--
ALTER TABLE `penjualan`
  ADD CONSTRAINT `penjualan_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `item` (`item_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
