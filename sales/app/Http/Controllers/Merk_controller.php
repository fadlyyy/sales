<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Datatables;
use DB;
use App\Models\Merk;

class Merk_controller extends Controller
{
    public function index(){
    	$title = 'List Merk';

    	return view('merk.merk_index',compact('title'));
    }

    public function add(){
    	$title = 'Tambah Merk';

    	return view('merk.merk_add',compact('title'));
    }

    public function store(Request $request){
    	$this->validate($request, [
    		'nama'=>'required'
           ]);

    	// $mk = new Merk;
    	// $mk->merk_id = \Uuid::generate(4);
    	// $mk->nama = $request->nama;
    	// $mk->save();
    	Merk::insert([
    		'merk_id'=>\Uuid::generate(4),
    		'nama'=>$request->nama,
    		'created_at'=>date('Y-m-d H:i:s'),
    		'updated_at'=>date('Y-m-d H:i:s'),
           ]);

    	\Session::flash('pesan','Data berhasil Ditambah');
    	return redirect('merk');
    }

    public function edit($id){
    	$title = 'Edit Merk';
    	$dt = Merk::where('merk_id',$id)->first();

    	return view('merk.merk_edit',compact('title','dt'));
    }

    public function update(Request $request,$id){
    	$this->validate($request,[
    		'nama'=>'required'
           ]);

    	Merk::where('merk_id',$id)->update([
    		'nama'=>$request->nama,
            'updated_at'=>date('Y-m-d H:i:s')
            ]);

    	\Session::flash('pesan','Data berhasil diupdate');
    	return redirect('merk');
    }

    public function delete($id){
    	try{
            Merk::where('merk_id',$id)->delete();

            \Session::flash('sukses','Data berhasil dihapus');
        }catch(\Exception $e){
            \Session::flash('gagal',$e->getMessage());
        }

        return redirect('merk');
    }

    public function yajra(Request $request){
    	DB::statement(DB::raw('set @rownum=0'));
        $users = Merk::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'merk_id',
            'nama']);
        $datatables = Datatables::of($users)
        ->addColumn('action',function($merk){
            return '<center><a href="merk/'.$merk->merk_id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a> <a href="merk/'.$merk->merk_id.'" class="btn btn-hapus btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Hapus</a></center>';
        });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        return $datatables->make(true);
    }
}
