<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Pengeluaran_controller extends Controller
{
    public function index(){
    	$title = 'Input pengeluaran';

    	return view('pengeluaran.pengeluaran_index',compact('title'));
    }

    public function store(Request $request){
    	$this->validate($request,[
    		'nama'=>'required',
    		'harga'=>'required'
    	]);

    	try{

    		\DB::table('pengeluaran')->insert([
    			'pengeluaran_id'=>\Uuid::generate(4),
    			'nama'=>$request->nama,
    			'harga'=>$request->harga,
    			'created_at'=>date('Y-m-d'),
    			'updated_at'=>date('Y-m-d')
    		]);

    		\Session::flash('sukses','Pengeluaran berhasil ditambah');

    	}catch(\Exception $e){
    		\Session::flash('gagal',$e->getMessage());
    	}

    	return redirect('pengeluaran');
    }
}
