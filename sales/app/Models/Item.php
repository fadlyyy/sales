<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'item';
    public $primaryKey = 'item_id';
    protected $casts = ['item_id'=>'string'];
}
