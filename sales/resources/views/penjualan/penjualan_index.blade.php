@extends('layouts.master')

@section('content')

@if(count($data) > 0)
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-body">

				<a href="{{ url('penjualan/clear/'.$struk) }}" class="btn btn-warning btn-flat">Clear</a>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>item</th>
							<th>Qty</th>
							<th>Laba</th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $e=>$dt)
						<tr>
							<td>{{ $e+1 }}</td>
							<td>{{ $dt->item->nama }}</td>
							<td>{{ $dt->qty }}</td>
							<td>{{ $dt->laba }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endif

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="box">
			@if(Session::has('pesan'))
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-check"></i> Sukses!</h4>
				{{ Session::get('pesan') }}
			</div>
			@endif
			<div class="box-body">
				<table class="table myTable">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama</th>
						</tr>
					</thead>
					<tbody>
						@foreach($item as $e=>$it)
						<tr>
							<td>{{ $e+1 }}</td>
							<td>
								<a style="color: black;" href="{{ url('pilih_item/'.$struk.'/'.$it->item_id) }}">{{ $it->nama }}</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<div class="box">
			<div class="box-body">
				<a href="{{ url('penjualan/save/'.$struk) }}" class="btn btn-flat btn-success btn-block">Simpan Transaksi</a>
			</div>
		</div>
	</div>
</div>

@endsection