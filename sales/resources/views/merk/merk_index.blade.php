@extends('layouts.master')

@section('content')

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<h3>{{ $title }}</h3>
		<div class="box">
			<div class="box-body">

				@if(Session::has('sukses'))
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-check"></i> Sukses!</h4>
					{{ Session::get('sukses') }}
				</div>
				@endif

				@if(Session::has('gagal'))
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-ban"></i> Gagal!</h4>
					{{ Session::get('gagal') }}
				</div>
				@endif
				
				<table class="table table-merk table-stripped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th><center>Action</center></th>
						</tr>
					</thead>
				</table>

			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal modal-danger fade" id="modal-hapus">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Danger Modal</h4>
				</div>
				<div class="modal-body">
					<p>Hapus Merk ini?</p>
				</div>
				<div class="modal-footer">
					<!-- <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button> -->
					<form method="post" action="">
						{{ csrf_field() }}
						{{ method_field('delete') }}
						<button type="submit" class="btn btn-outline">Hapus</button>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>

	@endsection

	@section('scripts')

	<script type="text/javascript">
		$(document).ready(function(){

			var flash = "{{ Session::has('pesan') }}";
			if(flash){
				var pesan = "{{ Session::get('pesan') }}";
				alert(pesan);
			}

			$('.table-merk').DataTable({
				processing: true,
				serverSide: true,
				ajax: "{{ url('merk/yajra') }}",
				columns: [
	            // or just disable search since it's not really searchable. just add searchable:false
	            {data: 'rownum', name: 'rownum'},
	            {data: 'nama', name: 'nama'},
	            {data: 'action', name: 'action', orderable: false, searchable: false}
	            ]
	        });

			$('body').on('click','.btn-hapus',function(e){
				e.preventDefault();
				var url = $(this).attr('href');

				$('#modal-hapus').find('form').attr('action',url);
				$('#modal-hapus').modal();
			})

		})
	</script>

	@endsection