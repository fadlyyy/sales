@extends('layouts.master')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box box-solid">
			<div class="box-header with-border">
				<i class="fa fa-text-width"></i>

				<h3 class="box-title">Terima Jasa Pembuatan Website</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<blockquote>
					<p>Terima Jasa Pembuatan Website untuk level Menengah ke Bawah.</p>
					<small>WA : 0896-0849-8550 (WA)</cite></small>
					<small>Email : fadlyrifai95@gmail.com</cite></small>
				</blockquote>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>

@endsection

@section('scripts')



@endsection