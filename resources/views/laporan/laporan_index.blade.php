@extends('layouts.master')

@section('content')

<div class="row">
	<div class="col-md-4 col-md-offset-4">
		<div class="box">
			<div class="box-body">
				<form role="form" method="get" action="{{ url('laporan-tanggal') }}">
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Dari Tanggal</label>
							<input type="text" value="{{ $dari }}" name="tanggal1" class="form-control datepicker" id="exampleInputEmail1" placeholder="Dari Tanggal">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Sampai Tanggal</label>
							<input type="text" value="{{ $sampai }}" name="tanggal2" class="form-control datepicker" id="exampleInputPassword1" placeholder="Sampai Tanggal">
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Cek</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- <div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-body">
				
				<center>
					<div style="width: 60%;height: 50%">
						<canvas id="myChart"></canvas>
					</div>
				</center>

			</div>
		</div>
	</div>
</div> -->

<?php
$totLaba = 0;
$totKotor = 0;
$totModal = 0;
$totPengeluaran = 0;
?>

@if(isset($data))
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3>Penjualan</h3>
			</div>
			<div class="box-body">
				
				<table class="table table-stripped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th>Qty</th>
							<th>Modal</th>
							<th>Kotor</th>
							<th>Bersih</th>
							<th>Tanggal</th>
						</tr>
					</thead>
					<tbody>
						
						@foreach($data as $e=>$dt)
						<?php 
						$totLaba += $dt->laba; 
						$totKotor += $dt->price;
						$totModal += $dt->buy; 
						?>
						<tr>
							<td>{{ $e+1 }}</td>
							<td>{{ $dt->item->nama }}</td>
							<td>{{ $dt->qty }}</td>
							<td>Rp. {{ str_replace(',','.',number_format($dt->buy,0)) }}</td>
							<td>Rp. {{ str_replace(',','.',number_format($dt->price,0)) }}</td>
							<td>Rp. {{ str_replace(',','.',number_format($dt->laba,0)) }}</td>
							<td>{{ date('d-M-Y',strtotime($dt->created_at)) }}</td>
						</tr>
						@endforeach
						<tr>
							<td colspan="3"><center><h3><b><i>Total Laba</i></b></h3></center></td>
							<td><h3><b><i>Rp. {{ str_replace(',','.',number_format($totModal,0)) }}</i></b></h3></td>
							<td><h3><b><i>Rp. {{ str_replace(',','.',number_format($totKotor,0)) }}</i></b></h3></td>
							<td><h3><b><i>Rp. {{ str_replace(',','.',number_format($totLaba,0)) }}</i></b></h3></td>
						</tr>
					</tbody>
					<tbody>
						
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="box">
			<div class="box-header">
				<h3>Pengeluaran</h3>
			</div>
			<div class="box-body">
				
				<table class="table table-stripped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th>Harga</th>
						</tr>
					</thead>
					<tbody>
						
						@foreach($pengeluaran as $e=>$pl)
						<tr>
							<td>{{ $e+1 }}</td>
							<td>{{ $pl->nama }}</td>
							<td>Rp. {{ str_replace(',','.',number_format($pl->harga,0)) }}</td>
							<?php $totPengeluaran += $pl->harga; ?>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2"><center><b><i><h4>Total Pengeluaran</h4></i></b></center></td>
							<td><h4><b><i>Rp. {{ str_replace(',','.',number_format($totPengeluaran,0)) }}</i></b></h4></td>
						</tr>
					</tfoot>
					<tbody>
						
					</tbody>
				</table>

			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="box">
			<div class="box-header">
				<h3>Bersih - Pengeluaran</h3>
			</div>
			<div class="box-body">
				
				<table class="table table-stripped">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th>Harga</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Bersih</td>
							<td>Rp. {{ str_replace(',','.',number_format($totLaba,0)) }}</td>
						</tr>
						<tr>
							<td>1</td>
							<td>Pengeluaran</td>
							<td>Rp. {{ str_replace(',','.',number_format($totPengeluaran,0)) }}</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="2">
								<center><h4><b><i>Total</i></b></h4></center>
							</td>
							<td>
								<h4><b><i>Rp. {{ number_format($totLaba - $totPengeluaran,0) }}</i></b></h4>
							</td>
						</tr>
					</tfoot>
					<tbody>
						
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>

<!-- {{ $tanggal }} -->
@endif

@endsection

@section('scripts')

<script type="text/javascript" src="{{ asset('Chart.js') }}"></script>

<script>
	
</script>

@endsection