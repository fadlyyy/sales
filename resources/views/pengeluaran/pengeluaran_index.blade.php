@extends('layouts.master')

@section('content')

<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="box">
			<div class="box-body">

				@if(\Session::has('sukses'))
				<div class="alert alert-success alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-check"></i> Sukses!</h4>
					{{ \Session::get('sukses') }}.
				</div>
				@endif

				@if(\Session::has('gagal'))
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<h4><i class="icon fa fa-ban"></i> Gagal!</h4>
					{{ \Session::get('gagal') }}.
				</div>
				@endif
				
				<form role="form" method="post" action="{{ url('pengeluaran') }}">
					{{ csrf_field() }}
					<div class="box-body">
						<div class="form-group">
							<label for="exampleInputEmail1">Nama Pengeluaran</label>
							<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nama Pengeluaran" name="nama">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Harga</label>
							<input type="number" class="form-control" id="exampleInputPassword1" placeholder="Harga" name="harga">
						</div>
					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<button type="submit" class="btn btn-primary">Input Pengeluaran</button>
					</div>
				</form>

			</div>
		</div>
	</div>
</div>

@endsection