<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Merk extends Model
{
    protected $table = 'merk';
    public $primaryKey = 'merk_id';
    protected $casts = ['merk_id'=>'string'];
}
