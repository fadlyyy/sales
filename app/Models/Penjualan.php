<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    protected $table = 'penjualan';
    public $primaryKey = 'penjualan_id';
    protected $casts = ['penjualan_id'=>'string'];

    public function item(){
    	return $this->belongsTo('App\Models\Item','item_id','item_id');
    }
}
