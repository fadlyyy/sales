<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Item;
use App\Models\Penjualan;

class Penjualan_controller extends Controller
{
    public function index($struk){
    	$title = 'Penjualan';
    	$struk = $struk;
    	$item = Item::where('stock','>',0)->get();
    	$data = Penjualan::where('struk',$struk)->get();

    	return view('penjualan.penjualan_index',compact('title','struk','item','data'));
    }

    public function pilih_item($struk,$item){
    	try{

            if(Penjualan::where('struk',$struk)->count() < 1){
                Penjualan::whereNull('penjualan')->delete();
            }

            $buy = Item::where('item_id',$item)->value('buy');
            $price = Item::where('item_id',$item)->value('price');
            $stockAwal = Item::where('item_id',$item)->value('stock');
            $laba = $price - $buy;

            $cek = Penjualan::where('item_id',$item)->where('struk',$struk)->count();
            if($cek > 0){
                $qtyNow = Penjualan::where('struk',$struk)->where('item_id',$item)->value('qty');

                \DB::transaction(function() use($struk,$item,$qtyNow,$laba,$stockAwal) {
                    Penjualan::where('struk',$struk)->where('item_id',$item)->update([
                        'qty'=>$qtyNow+1,
                        'laba'=>($laba) * ($qtyNow+1)
                    ]);
                });

            }else{

                \DB::transaction(function() use($struk,$item,$buy,$price,$laba,$stockAwal) {

                    Penjualan::insert([
                        'penjualan_id'=>\Uuid::generate(4),
                        'struk'=>$struk,
                        'item_id'=>$item,
                        'qty'=>1,
                        'buy'=>$buy,
                        'price'=>$price,
                        'laba'=>$laba,
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ]);

                });

            }

        }catch(\Exception $e){
            dd($e->getMessage());
        }

        return redirect('penjualan/'.$struk);
    }

    public function clear($struk){
    	Penjualan::whereNull('penjualan')->delete();
    	return redirect('penjualan/'.$struk);
    }

    public function simpan_transaksi($struk){
    	// Penjualan::where('struk',$struk)->update([
    		// 'penjualan'=>1
           // ]);

        $penjualan = Penjualan::where('struk',$struk)->select('penjualan_id','item_id')->get();

        foreach ($penjualan as $pj) {

            $stockAwal = Item::where('item_id',$pj->item_id)->value('stock');

            Item::where('item_id',$pj->item_id)->update([
                'stock'=>$stockAwal - 1
            ]);

            Penjualan::where('penjualan_id',$pj->penjualan_id)->update([
                'penjualan'=>1
            ]);

        }

        \Session::flash('pesan','Transaksi berhasil di simpan');
        return redirect('penjualan/'.\Uuid::generate(4));
    }
}
