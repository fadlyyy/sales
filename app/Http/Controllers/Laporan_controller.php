<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Penjualan;

class Laporan_controller extends Controller
{
    public function index(){
    	$title = 'Laporan Penjualan (Default Hari ini)';
        $data = Penjualan::whereDay('created_at',date('d'))->where('penjualan',1)->orderBy('created_at','desc')->get();
        $pengeluaran = \DB::table('pengeluaran')->where('created_at',date('Y-m-d'))->get();

        $tanggal = array();
        $nilai = array();
        foreach ($data as $dt) {
            array_push($tanggal, date('Y-m-d',strtotime($dt->created_at)));
            $hitung = Penjualan::where('created_at',$dt->created_at)->count();
            array_push($nilai, $hitung);
        }
        $tanggal = json_encode($tanggal);
        $nilai = json_encode($nilai);

        $dari = date('Y-m-d');
        $sampai = date('Y-m-d');

        // dd($nilai);

    	return view('laporan.laporan_index',compact('title','data','tanggal','nilai','pengeluaran','dari','sampai'));
    }

    public function tanggal(Request $request){
    	$tanggal1 = date('Y-m-d',strtotime($request->tanggal1));
    	$tanggal2 = date('Y-m-d',strtotime($request->tanggal2));
    	$title = "Laporan Penjualan dari Tanggal ".date('d-M-Y',strtotime($tanggal1))." sampai Tanggal ".date('d-M-Y',strtotime($tanggal2));
    	$data = Penjualan::whereBetween('created_at',[$tanggal1,$tanggal2])->where('penjualan',1)->orderBy('created_at','desc')->get();
        $pengeluaran = \DB::table('pengeluaran')->whereBetween('created_at',[$tanggal1,$tanggal2])->get();

        $tanggal = array();
        foreach ($data as $dt) {
            array_push($tanggal, $dt->created_at);
        }
        $tanggal = json_encode($tanggal);

        $dari = $tanggal1;
        $sampai = $tanggal2;

    	return view('laporan.laporan_index',compact('title','data','tanggal','pengeluaran','dari','sampai'));
    }
}
