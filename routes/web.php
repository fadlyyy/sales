<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware'=>'auth'],function(){
	Route::get('jasa',function(){
		$title = 'Jasa Pembuatan Website';
		return view('jasa.jasa_index',compact('title'));
	});

	Route::get('/', function () {
		return redirect('beranda');
	});

	Route::get('beranda','Beranda_controller@index');

// Merk
	Route::get('merk','Merk_controller@index');
	Route::get('merk/yajra','Merk_controller@yajra');
	Route::get('merk/add','Merk_controller@add');
	Route::post('merk/store','Merk_controller@store');
	Route::get('merk/{id}','Merk_controller@edit');
	Route::put('merk/{id}','Merk_controller@update');
	Route::delete('merk/{id}','Merk_controller@delete');

// Supplier
	Route::get('supplier','Supplier_controller@index');
	Route::get('supplier/yajra','Supplier_controller@yajra');
	Route::get('supplier/add','Supplier_controller@add');
	Route::post('supplier/store','Supplier_controller@store');
	Route::get('supplier/{id}','Supplier_controller@edit');
	Route::put('supplier/{id}','Supplier_controller@update');
	Route::delete('supplier/{id}','Supplier_controller@delete');

// Kategori
	Route::get('kategori','Kategori_controller@index');
	Route::get('kategori/yajra','Kategori_controller@yajra');
	Route::get('kategori/add','Kategori_controller@add');
	Route::post('kategori/store','Kategori_controller@store');
	Route::get('kategori/{id}','Kategori_controller@edit');
	Route::put('kategori/{id}','Kategori_controller@update');
	Route::delete('kategori/{id}','Kategori_controller@delete');

// Item
	Route::get('item','Item_controller@index');
	Route::get('item/yajra','Item_controller@yajra');
	Route::get('item/add','Item_controller@add');
	Route::post('item/store','Item_controller@store');
	Route::get('item/{id}','Item_controller@edit');
	Route::put('item/{id}','Item_controller@update');
	Route::delete('item/{id}','Item_controller@delete');
	Route::post('item/clone/{id}','Item_controller@cloning');

// Penjualan
	Route::get('penjualan/{struk}','Penjualan_controller@index');
	Route::get('pilih_item/{struk}/{item}','Penjualan_controller@pilih_item');
	Route::get('penjualan/clear/{struk}','Penjualan_controller@clear');
	Route::get('penjualan/save/{struk}','Penjualan_controller@simpan_transaksi');

// Pengeluaran
	Route::get('pengeluaran','Pengeluaran_controller@index');
	Route::post('pengeluaran','Pengeluaran_controller@store');

// Laporan
	Route::get('laporan','Laporan_controller@index');
	Route::get('laporan-tanggal','Laporan_controller@tanggal');
	Route::get('chart');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
